create table applog(
    id serial       primary key,
	message         text,
    context         text,
	level           varchar(255),
	level_name      varchar(255),
	channel         varchar(255),
	record_datetime timestamp without time zone,
	extra           text,
	formatted       text,
	remote_addr     varchar(255),
	user_agent      varchar(255),
	created_at      timestamp without time zone default now(),
	modified_at     timestamp without time zone default null
);